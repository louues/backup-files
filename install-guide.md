Wifi internet

    iwctl
    device list
    station "name" scan
    station "name" get-networks
    station "name" connect "network-name"
    exit

Test internet

    ping archlinux.org

Make partitions

    fdisk -l
    fdisk /dev/(Partitions)
	g
	n, +300M, t1 #boot partition
	n #root partition
	w
    mkfs.fat -F32 /dev/boot
    mkfs.ext4 /dev/root

Mount partitions

    mount /dev/root /mnt
    mkdir /mnt/boot
    mkdir /mnt/boot/efi
    mount /dev/boot /mnt/boot/efi

Generate fstab file

    mkdir /mnt/etc
    genfstab -U -p /mnt >> /mnt/etc/fstab
    cat /mnt/etc/fstab

Install basic system

    pacstrap -i /mnt base base-devel linux linux-firmware linux-headers vim

Login the system

    arch-chroot /mnt

Locale settings

    ln -sf /usr/share/zoneinfo/Brazil/West /etc/localtime
    hwclock --systohc
    vim /etc/locale.gen #uncomment en_US pt_BR
    locale-gen

User settings

    echo archpc >> /etc/hostname
    vim /etc/hosts
	    127.0.0.1	localhost
	    ::1		localhost
	    127.0.1.1	archpc.localdomain	archpc
    echo KEYMAP=keyboard >> /etc/vconsole.conf #if not default keyboard

    passwd
    useradd -m -g users -G wheel luist 
    usermod -aG wheel,audio,video,optical,storage luist 
    passwd user
    pacman -S sudo
    EDITOR=vim visudo #Uncomment to allow members of group wheel to execute any command

Removable Device

    vim /etc/mkinitcpio.conf #HOOKS block keyboard (before autodetect)
    mkinitcpio -p linux

Grub

    pacman -S grub efibootmgr dosfstools os-prober mtools
    grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=grub_uefi (--removable) --recheck
    cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo
    grub-mkconfig -o /boot/grub/grub.cfg

Network

    pacman -S networkmanager
    systemctl enable NetworkManager

Exit

    exit
    umount -l /mnt
    reboot

