INSTALL ENVIROMENT:

Install Pacman Programs

    sudo vim /etc/pacman.conf
```include multilib```

Pacman system requirements

    sudo pacman -S xorg xorg-server xorg-server-devel openssh intel-ucode nvidia nvidia-utils nvidia-settings opencl-nvidia git ntfs-3g xfce4 alacritty firefox

Install ly display manager

    mkdir ~/git
    git clone --recurse-submodules https://github.com/fairyglade/ly ~/git/
    cd ~/git/ly
    make
    sudo make install installsystemd
    sudo systemctl enable ly.service
    reboot

INSTALL PROGRAMS:

Pacman main programs

    sudo pacman -S neovim fish pcmanfm nitrogen discord libreoffice thunderbird gimp ranger htop ncdu exa tree locate picom pulseaudio pulseaudio-alsa alsa-utils 

Pacman extra progams

    sudo pacman -S neofetch python-pip gedit mupdf loupe qalculate-gtk geogebra flameshot sl zip unzip unrar p7zip bitwarden qbittorrent vlc xclip

Pacman games

    sudo pacman -S wine steam ttf-liberation lutris 

Install yay

    git clone https://aur.archlinux.org/yay.git ~/git/
    cd ~/git/yay
    makepkg -si

Yay programs

    yay -S pfetch sublime-text-4 whatsdesk-bin peazip-qt-bin woeusb minecraft-launcher 

CONFIGURATION:

Configure ssh key

    git config --global user.name "luist"
    git config --global user.email taborda.lmf@gmail.com
    ssh-keygen -t rsa -b 2048 -C "taborda.lmf@gmail.com"
```copy key in ~/.ssh/id_rsa.pub and paste in gitlab.com -> Edit profile -> SSH keys```

    sudo rm -r ~/git/backup-files
    git clone git@gitlab.com:louues/backup-files.git ~/git/ 

Config Files

Terminal

    cp ~/git/backup-files/dotbashrc ~/.bashrc

Fish

    cp -r ~/git/backup-files/dotconfig/fish/functions ~/.config/fish/functions

Alacritty

    cp ~/git/backup-files/dotconfig/alacritty.toml ~/.config/alacritty.toml
    git clone https://github.com/alacritty/alacritty-theme.git ~/git/ 


Configure Printer

    sudo pacman -S cups cups-filters cups-pdf gutenprint foomatic-db-gutenprint-ppds ghostscript gsfonts libcups simple-scan 
    sudo systemctl enable cups
    sudo systemctl start cups
```In Browser -> localhost:631 -> Administration -> Add Printer```


