import math

print("Cálculos vetoriais.")
#Número das opcões
sair = 0
vetor_com_2_pontos = 1
soma_vetores = 2
produto_por_escalar = 3
modulo_vetor = 4
produto_escalar = 5
projecao_ortogonal = 6
produto_vetorial = 7
produto_misto = 8
seno_e_cosseno_entre_vetores = 9
vetores_paralelos_ou_perpendiculares = 10
vetores_coplanares = 11
area_2_vetores = 12
volume_3_vetores = 13
dependencia_linear = 14

#Funcões
def modulo_numero(numero): #Recebe float
    if (numero < 0):
        return -numero
    else:
        return numero #Retorna float
def formatar_sintaxe_vetor(vetor): #Recebe um array
    vetor = "(" + str(round((vetor[0]), 2)) + ", " + str(round((vetor[1]), 2)) + ", " + str(round((vetor[2]), 2)) + ")"
    return vetor #Retorna uma string
def calc_vetor_com_2_pontos(ponto1, ponto2): #Recebe dois arrays
    x1 = float(ponto1[0])
    y1 = float(ponto1[1])
    z1 = float(ponto1[2])
    x2 = float(ponto2[0])
    y2 = float(ponto2[1])
    z2 = float(ponto2[2])
    x = x2 - x1
    y = y2 - y1
    z = z2 - z1
    vetor = [x, y, z]
    return vetor #Retorna um array
def calc_modulo_vetor(vetor): #Recebe um array
    x = float(vetor[0])
    y = float(vetor[1])
    z = float(vetor[2])
    modulo = round((((x ** 2) + (y ** 2) + (z ** 2)) ** 0.5), 2)
    return modulo #Retorna um float
def calc_soma_vetores(vetor1, vetor2): #Recebe dois arrays
    x1 = float(vetor1[0])
    y1 = float(vetor1[1])
    z1 = float(vetor1[2])
    x2 = float(vetor2[0])
    y2 = float(vetor2[1])
    z2 = float(vetor2[2])
    x = x1 + x2
    y = y1 + y2
    z = z1 + z2
    soma = [x, y, z]
    return soma #Retorna um array
def calc_produto_por_escalar(vetor, multiplo): #Recebe um array e um float
    x = float(vetor[0])
    y = float(vetor[1])
    z = float(vetor[2])
    x = x * multiplo
    y = y * multiplo
    z = z * multiplo
    produto = [x, y, z]
    return produto #Retorna um array
def calc_produto_escalar(vetor1, vetor2): #Recebe dois arrays
    x1 = float(vetor1[0])
    y1 = float(vetor1[1])
    z1 = float(vetor1[2])
    x2 = float(vetor2[0])
    y2 = float(vetor2[1])
    z2 = float(vetor2[2])
    produto = (x1 * x2) + (y1 * y2) + (z1 * z2)
    return produto #Retorna um float
def calc_produto_vetorial(vetor1, vetor2): #Recebe dois arrays
    x1 = float(vetor1[0])
    y1 = float(vetor1[1])
    z1 = float(vetor1[2])
    x2 = float(vetor2[0])
    y2 = float(vetor2[1])
    z2 = float(vetor2[2])
    x = (y1 * z2) - (z1 * y2)
    y = (z1 * x2) - (x1 * z2)
    z = (x1 * y2) - (y1 * x2)
    produto = [x, y, z]
    return produto #Retorna um array
def calc_produto_misto(vetor1, vetor2, vetor3): #Recebe 3 arrays
    x1 = float(vetor1[0])
    y1 = float(vetor1[1])
    z1 = float(vetor1[2])
    x2 = float(vetor2[0])
    y2 = float(vetor2[1])
    z2 = float(vetor2[2])
    x3 = float(vetor3[0])
    y3 = float(vetor3[1])
    z3 = float(vetor3[2])
    x = (x1 * y2 * z3) - (x1 * z2 * y3)
    y = (y1 * z2 * x3) - (y1 * x2 * z3)
    z = (z1 * x2 * y3) - (z1 * y2 * x3)
    produto = x + y + z
    return produto #Retorna um float
def calc_projecao_ortogonal(vetor_principal, vetor_base): #Recebe 2 arrays
    x1 = float(vetor_principal[0])
    y1 = float(vetor_principal[1])
    z1 = float(vetor_principal[2])
    x2 = float(vetor_base[0])
    y2 = float(vetor_base[1])
    z2 = float(vetor_base[2])
    produto_escalar = calc_produto_escalar(vetor_principal, vetor_base)
    vetor_base_quadrado = calc_modulo_vetor(vetor_base) ** 2
    termo1 = produto_escalar / vetor_base_quadrado
    projecao = calc_produto_por_escalar(vetor_base, termo1)
    return projecao
def calc_seno_entre_vetores(vetor1, vetor2): #Recebe 2 arrays
    produto_vetorial = calc_produto_vetorial(vetor1, vetor2)
    modulo_produto = calc_modulo_vetor(produto_vetorial)
    produto_modulos = calc_modulo_vetor(vetor1) * calc_modulo_vetor(vetor2)
    seno = round((modulo_produto / produto_modulos), 2)
    return seno #Retorna um float
def calc_cosseno_entre_vetores(vetor1, vetor2): #Recebe 2 arrays
    produto_escalar = calc_produto_escalar(vetor1, vetor2)
    produto_modulos = calc_modulo_vetor(vetor1) * calc_modulo_vetor(vetor2)
    cosseno = round(((produto_escalar / produto_modulos)), 2)
    return cosseno #Retorna um float
def calc_paralelos(vetor1, vetor2): #Recebe 2 arrays
    if (calc_modulo_vetor(calc_produto_vetorial(vetor1, vetor2)) == 0):
        return True
    else:
        return False
def calc_perpendiculares(vetor1, vetor2): #Recebe 2 arrays
    if (calc_produto_escalar(vetor1, vetor2) == 0):
        return True
    else:
        return False
def calc_coplanares(vetor1, vetor2, vetor3): #Recebe 3 arrays
    if (calc_produto_misto(vetor1, vetor2, vetor3) == 0):
        return True
    else:
        return False
def calc_area_2_vetores(vetor1, vetor2): #Recebe 2 arrays
    area = calc_modulo_vetor(calc_produto_vetorial(vetor1, vetor2))
    return area #Retorna float
def calc_volume_3_vetores(vetor1, vetor2, vetor3): #Recebe 3 arrays
    volume = modulo_numero(calc_produto_misto(vetor1, vetor2, vetor3))
    return volume #Retorna float
def calc_dependencia_linear_2_vetores(vetor1, vetor2): #Recebe 2 arrays
    if (calc_modulo_vetor(calc_produto_vetorial(vetor1, vetor2)) == 0):
        return True
    else:
        return False
def calc_dependencia_linear_3_vetores(vetor1, vetor2, vetor3): #Recebe 3 arrays
    if (calc_produto_misto(vetor1, vetor2, vetor3) == 0):
        return True
    else:
        return False

while True:
    #Texto
    print("1 - Calcular o vetor a partir de 2 pontos")
    print("2 - Calcular soma de dois vetores")
    print("3 - Calcular produto por escalar de um vetor")
    print("4 - Calcular módulo de um vetor")
    print("5 - Calcular produto escalar de 2 vetores")
    print("6 - Calcular projecão ortogonal de um vetor principal sobre um vetor base")
    print("7 - Calcular produto vetorial de 2 vetores")
    print("8 - Calcular produto misto de 3 vetores")
    print("9 - Calcular seno e cosseno entre 2 vetores")
    print("10 - Calcular se 2 vetores são paralelos, perpendiculares ou nenhum dos dois")
    print("11 - Calcular se 3 vetores são coplanares")
    print("12 - Calcular a área do paralelogramo formado por 2 vetores")
    print("13 - Calcular o volume do paralelepípedo formado por 3 vetores")
    print("14 - Calcular dependência linear de 2 ou 3 vetores")
    print("0 - Sair")
    opcao = int(input("Escolha a opcão: "))

    #Opcões
    if (opcao == vetor_com_2_pontos):
        p1 = input("Ponto 1(x, y, z): ").split(",")
        p2 = input("Ponto 2(x, y, z): ").split(",")
        vetor = calc_vetor_com_2_pontos(p1, p2)
        vetor = formatar_sintaxe_vetor(vetor)
        print("Vetor: " + vetor)
        print("\n")
    elif (opcao == soma_vetores):
        v1 = input("Vetor 1(x, y, z): ").split(",")
        v2 = input("Vetor 2(x, y, z): ").split(",")
        soma = calc_soma_vetores(v1, v2)
        soma = formatar_sintaxe_vetor(soma)
        print("Soma dos vetores: " + str(soma))
        print("\n")
    elif (opcao == produto_por_escalar):
        v = input("Vetor(x, y, z): ").split(",")
        m = float(input("Número multiplicador: "))
        produto = calc_produto_por_escalar(v, m)
        produto = formatar_sintaxe_vetor(produto)
        print("Produto: " + produto)
        print("\n")
    elif (opcao == modulo_vetor):
        v = input("Vetor(x, y, z): ").split(",")
        modulo = calc_modulo_vetor(v)
        print("Módulo: " + str(modulo))
        print("\n")
    elif (opcao == produto_escalar):
        v1 = input("Vetor 1(x, y, z): ").split(",")
        v2 = input("Vetor 2(x, y, z): ").split(",")
        produto = calc_produto_escalar(v1, v2)
        print("Produto escalar: " + str(produto) + " u")
        print("\n")
    elif (opcao == projecao_ortogonal):
        vp = input("Vetor principal(x, y, z): ").split(",")
        vb = input("Vetor base(x, y, z): ").split(",")
        projecao = calc_projecao_ortogonal(vp, vb)
        projecao = formatar_sintaxe_vetor(projecao)
        print("Projecão ortogonal: " + projecao)
        print("\n")
    elif (opcao == produto_vetorial):
        v1 = input("Vetor 1(x, y, z): ").split(",")
        v2 = input("Vetor 2(x, y, z): ").split(",")
        produto = calc_produto_vetorial(v1, v2)
        produto = formatar_sintaxe_vetor(produto)
        print("Produto vetorial: " + produto)
        print("\n")
    elif (opcao == produto_misto):
        v1 = input("Vetor 1(x, y, z): ").split(",")
        v2 = input("Vetor 2(x, y, z): ").split(",")
        v3 = input("Vetor 3(x, y, z): ").split(",")
        produto = calc_produto_misto(v1, v2, v3)
        print("Produto misto: " + str(produto))
        print("\n")
    elif (opcao == seno_e_cosseno_entre_vetores):
        v1 = input("Vetor 1(x, y, z): ").split(",")
        v2 = input("Vetor 2(x, y, z): ").split(",")
        seno = calc_seno_entre_vetores(v1, v2)
        cosseno = calc_cosseno_entre_vetores(v1,v2)
        print("Seno: " + str(seno))
        print("Cosseno: " + str(cosseno))
        print("\n")
    elif (opcao == vetores_paralelos_ou_perpendiculares):
        v1 = input("Vetor 1(x, y, z): ").split(",")
        v2 = input("Vetor 2(x, y, z): ").split(",")
        if (calc_paralelos(v1, v2)):
            print("Os vetores são paralelos")
        elif (calc_perpendiculares(v1, v2)):
            print("Os vetores são perpendiculares")
        else:
            print("Os vetores não são paralelos nem perpendiculares")
        print("\n")
    elif (opcao == vetores_coplanares):
        v1 = input("Vetor 1(x, y, z): ").split(",")
        v2 = input("Vetor 2(x, y, z): ").split(",")
        v3 = input("Vetor 2(x, y, z): ").split(",")
        if calc_coplanares(v1, v2, v3):
            print("Os vetores são coplanares")
        else:
            print("Os vetores não são coplanares")
        print("\n")
    elif (opcao == area_2_vetores):
        v1 = input("Vetor 1(x, y, z): ").split(",")
        v2 = input("Vetor 2(x, y, z): ").split(",")
        area = calc_area_2_vetores(v1, v2)
        print("Área: " + str(area))
        if (area == 0):
            print("Os vetores são paralelos e não formam um paralelogramo")
        print("\n")
    elif (opcao == volume_3_vetores):
        v1 = input("Vetor 1(x, y, z): ").split(",")
        v2 = input("Vetor 2(x, y, z): ").split(",")
        v3 = input("Vetor 2(x, y, z): ").split(",")
        volume = calc_volume_3_vetores(v1, v2, v3)
        print("Volume: " + str(volume))
        if (volume == 0):
            print("Os vetores são coplanares e não formam um paralelepípedo")
        print("\n")
    elif (opcao == dependencia_linear):
        qtd_vetores = int(input("2 ou 3 vetores: "))
        if (qtd_vetores == 2):
            v1 = input("Vetor 1(x, y, z): ").split(",")
            v2 = input("Vetor 2(x, y, z): ").split(",")
            dependente = calc_dependencia_linear_2_vetores(v1, v2)
            if dependente:
                print("Os vetores são dependentes")
            else:
                print("Os vetores são independentes")
        elif (qtd_vetores == 3):
            v1 = input("Vetor 1(x, y, z): ").split(",")
            v2 = input("Vetor 2(x, y, z): ").split(",")
            v3 = input("Vetor 2(x, y, z): ").split(",")
            dependente = calc_dependencia_linear_3_vetores(v1, v2, v3)
            if dependente:
                print("Os vetores são dependentes")
            else:
                print("Os vetores são independentes")
        print("\n")
    elif (opcao == sair):
        break
    else:
        print("Opcão não existe")
    
    #Continuar loop
    print("1 - Continuar")
    print("2 - Sair")
    continuar = int(input())
    if (continuar == 2):
        break
    else:
        pass
