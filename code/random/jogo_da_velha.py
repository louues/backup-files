class JogoDaVelha:
    def __init__(self):
        self.elements = []
        self.vez_jogador1 = True
        self.ganhador = None
        self.simbolo = "^"
        self.jog1_simbolo = "X"
        self.jog2_simbolo = "O"

    def jogar(self) -> None:
        self.gerar_elements()
        while True:
            self.printar()
            self.jogada()
            if self.vitoria():
                self.printar()
                print(self.ganhador, "ganhou o jogo")
                break
            if self.empate():
                self.printar()
                print("Deu velha :(")
                break

    def gerar_elements(self) -> None:
        for i in range(3):
            temp_jogo = []
            for j in range(3):
                temp_jogo.append(self.simbolo)
            self.elements.append(temp_jogo)

    def printar(self) -> None:
        print("   ", end="") #Números coluna de cima
        for i in range(3): 
            print(i+1, " ", end="") 
        print("\n")

        for i in range(len(self.elements)): #Númeors coluna do lado
            print(i+1, " ", end="")  
            for j in range(len(self.elements[i])): #jogo
                print(self.elements[i][j], " ", end="") 
            print("\n")

    def jogada(self) -> None:
        posicao = self.ler_input()
        self.posicionar_jogada(posicao)

    def ler_input(self) -> list:
        print("Escolha um lugar (x,y) para colocar ", end="")
        if self.vez_jogador1:
            print(self.jog1_simbolo)
            posicao = input().split(",")
        else:
            print(self.jog2_simbolo)
            posicao = input().split(",")
        print("\n")
        return posicao

    def posicionar_jogada(self, posicao) -> None:
        x = int(posicao[0]) - 1
        y = int(posicao[1]) - 1
        if not self.posicao_livre(x, y):
            print("Essa posicão já está ocupada, tente novamente!\n")
            return

        if self.vez_jogador1:
            self.elements[x][y] = self.jog1_simbolo
            self.vez_jogador1 = False
        else:
            self.elements[x][y] = self.jog2_simbolo
            self.vez_jogador1 = True

    def posicao_livre(self, x, y) -> bool:
        if self.elements[x][y] == self.simbolo:
            return True
        return False

    def linha_diagonal_completa(self) -> bool:
        p1 = self.elements[0][0]
        p3 = self.elements[0][2]
        p5 = self.elements[1][1]
        p7 = self.elements[2][0]
        p9 = self.elements[2][2]
        if p1 == p5 == p9: # esquerda -> direita
            if p1 == self.jog1_simbolo:
                self.ganhador = self.jog1_simbolo
                return True
            elif p1 == self.jog2_simbolo:
                self.ganhador = self.jog2_simbolo
                return True
        elif p3 == p5 == p7: # direita -> esquerda
            if p3 == self.jog1_simbolo:
                self.ganhador = self.jog1_simbolo
                return True
            elif p3 == self.jog2_simbolo:
                self.ganhador = self.jog2_simbolo
                return True

    def linha_horizontal_completa(self) -> bool:
        for i in range(len(self.elements)):
            if (self.elements[i][0] == self.elements[i][1] == self.elements[i][2]):
                if self.elements[i][0] == self.jog1_simbolo:
                    self.ganhador = self.jog1_simbolo
                    return True
                if self.elements[i][0] == self.jog2_simbolo:
                    self.ganhador = self.jog2_simbolo
                    return True
        return False

    def linha_vertical_completa(self) -> bool:
        for i in range(len(self.elements)):
            if (self.elements[0][i] == self.elements[1][i] == self.elements[2][i]):
                if self.elements[0][i] == self.jog1_simbolo:
                    self.ganhador = self.jog1_simbolo
                    return True
                elif self.elements[0][i] == self.jog2_simbolo:
                    self.ganhador = self.jog2_simbolo
                    return True
        return False

    def vitoria(self) -> bool:
        if self.linha_diagonal_completa():
            return True
        if self.linha_horizontal_completa():
            return True
        if self.linha_vertical_completa():
            return True
        return False

    def empate(self) -> bool:
        for i in range(len(self.elements)):
            for j in range(len(self.elements[i])):
                if self.elements[i][j] == self.simbolo:
                    return False
        return True

def jogar_novamente() -> bool:
    print("\nJogar novamente?")
    print("1 - Sim \n2- Sair")
    jogar_nov = int(input())
    if jogar_nov == 1:
        return True
    return False

def main() -> None:
    while True:
        jogo = JogoDaVelha()
        jogo.jogar()
        if not jogar_novamente():
            break

if __name__ == "__main__":
    main()