import time
import threading
from pynput.mouse import Controller, Button
from pynput.keyboard import Listener, KeyCode

TOGGLE_KEY = KeyCode(char="t")
delay = 0.1

#Animation
str1 = "."
str2 = ".."
str3 = "..."
i = 0


clicking = False
mouse = Controller()

def clicker():
    while True:
        global i
        mod = i % 3
        if clicking:
            mouse.click(Button.left, 1)
            if mod == 0:
                print(str1, "\r", end="")
            elif mod == 1:
                print(str2, "\r", end="")
            else:
                print(str3, "\r", end="")
            i += 1
        time.sleep(delay)

def toggle_event(key):
    if key == TOGGLE_KEY:
        global clicking
        clicking = not clicking

click_thread = threading.Thread(target=clicker)
click_thread.start()

with Listener(on_press=toggle_event) as listener:
    listener.join()

