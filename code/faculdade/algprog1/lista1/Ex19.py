print("Calcular quantidade de tinta.")

area = int(input("Área a ser pintada(m²): "))

qtd_tinta = area / 3
qtd_latas = round((qtd_tinta / 18) + 0.5)
qtd_galoes = round((qtd_tinta / 3.6) + 0.5)
preco_latas = qtd_latas * 80
preco_galoes = qtd_galoes * 25

qtd_latas_misto = round((qtd_tinta / 18) - 0.5)
qtd_galoes_misto =  round(((qtd_tinta % 18) / 3.6) + 0.5)
preco_latas_misto = qtd_latas_misto * 80
preco_galoes_misto = qtd_galoes_misto * 25
preco_misto = preco_latas_misto + preco_galoes_misto

print("Quantidade apenas em latas: " + str(qtd_latas))
print("Preco apenas em latas: " + str(preco_latas))
print("Quantidade apenas em galões: " + str(qtd_galoes))
print("Preco apenas em galões: " + str(preco_galoes))
print("Quantidade mista: " + str(qtd_latas_misto) + " latas e " + str(qtd_galoes_misto) + " galoes")
print("Preco misto: " + str(preco_misto))