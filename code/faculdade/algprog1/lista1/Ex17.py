print("Informacões do salário.")

recebe = float(input("Quanto você recene por hora?: "))
horas = float(input("Quanto horas você trabalha por mês?: "))

salario =  round((recebe * horas), 2)
imposto_de_renda = round((salario * 0.11), 2)
inss = round((salario * 0.08 ), 2)
sindicato = round((salario * 0.05), 2)
salario_final = round((salario * 0.76), 2)

print("Salário bruto: " + str(salario))
print("Desconto do imposto de renda: " + str(imposto_de_renda))
print("Desconto do INSS: " + str(inss))
print("Desconto do sindicato: " + str(sindicato))
print("Salário líquido: " + str(salario_final))
