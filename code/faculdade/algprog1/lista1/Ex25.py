print("Calcular combustível gasto.")

tempo = int(input("Tempo gasto(h): "))
velocidade = int(input("Velocidade média(km/h): "))

distancia = tempo * velocidade
combustivel_gasto = round((distancia / 12), 2)

print("Combustível gasto: " + str(combustivel_gasto) + " litros")