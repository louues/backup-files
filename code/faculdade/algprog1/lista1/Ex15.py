print("Mostrar peso recomendado por altura.")

altura = int(input("Digite sua altura(cm): "))

peso_m = round(((72.7 * (altura * 0.01)) - 58), 2)
peso_f = round(((62.1 * (altura * 0.01)) - 44.7), 2)

print("O seu peso recomendado é " + str(peso_m) + " kg se for homem e " + str(peso_f) + " kg se for mulher")