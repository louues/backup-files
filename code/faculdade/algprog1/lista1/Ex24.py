print("Calcular programa de um hotel.")

num_apartamentos = int(input("Número de apartamentos: "))
valor_diaria = int(input("Valor da diária: "))

valor_diaria_promo = valor_diaria * 0.75

valor_lotado_promo = num_apartamentos * valor_diaria_promo
valor_70pc_promo = (num_apartamentos * 0.70) * valor_diaria_promo

valor_lotado_total = num_apartamentos * valor_diaria
valor_perdido = valor_lotado_total - valor_lotado_promo

print("Valor promocional da diária: " + str(valor_diaria_promo))
print("Valor arrecadada caso lotacão de 100%: " + str(valor_lotado_promo))
print("Valor arrecadado caso lotacão de 70%: " + str(valor_70pc_promo))
print("Valor perdido na promocão: " + str(valor_perdido))