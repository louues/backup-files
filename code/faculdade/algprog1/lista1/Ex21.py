print("Tempo médio entre dois pontos.")

distancia_km = float(input("Distância(km): "))
velocidade_kmh = float(input("Velocidade(km/h): "))

tempo_horas = distancia_km / velocidade_kmh
velocidade_ms = round((velocidade_kmh / 3.6), 2)

print("Tempo gasto: " + str(tempo_horas))
print("Velocidade média: " + str(velocidade_ms) + " m/s")