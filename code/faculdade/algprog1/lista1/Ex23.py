print("Segundos para minutos, horas e dias.")

segundos = int(input("Informe um tempo em segundos: "))

minutos = round((segundos / 60), 2)
horas = round((minutos / 60), 2)
dias = round((horas / 24), 2)

print("O tempo é igual a " + str(minutos) + " minutos, " + str(horas) + " horas e " + str(dias) + " dias")
