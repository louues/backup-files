print("Calcular o quadrado e o cubo do número.")

num = int(input("Digite um número: "))

num_quadrado = num * num
num_cubo = num * num * num

print("O quadrado do número é " + str(num_quadrado) + " e o cubo é " + str(num_cubo))
