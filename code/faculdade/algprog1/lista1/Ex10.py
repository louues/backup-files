print("Dividir um número por 2 e 3.")

num = float(input("Digite um número: "))

div_2 = num / 2
div_3 = round((num / 3), 2)

print("O número dividido por 2 é " + str(div_2) + " e por 3 é " + str(div_3))