print("Calcular troco em notas.")

valor_compra = int(input("Valor da compra: "))
valor_recebido = int(input("Valor recebido: "))
troco = valor_recebido - valor_compra

n100 = int(troco / 100)
n50 = int((troco % 100) / 50)
n10 = int(((troco% 100) % 50) / 10)
n5 = int((((troco % 100) % 50) % 10 ) / 5)
n1 = ((((troco % 100) % 50) % 10) % 5)

t100 = str(n100) + " de 100 reais"
t50 = str(n50) + " de 50 reais"
t10 = str(n10) + " de 10 reais"
t5 = str(n5) + " de 5 reais"
t1 = str(n1) + " de 1 real"

print("O troco de " + str(troco) + " deverá ser composto das seguintes notas: " + t100 + ", " + t50 + ", " + t10 + ", " + t5 + " e " + t1) 


