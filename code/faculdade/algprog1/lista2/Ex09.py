print("Determinar imc.")

print("Peso: ")
peso = float(input())
print("Altura: ")
altura = float(input())

imc = peso / (altura * altura)
print("O seu imc é de " + str(imc))

if (imc < 20):
    print("Você está abaixo do peso")
elif (imc <= 25):
    print("Você está com um peso normal")
elif (imc <= 30):
    print("Você está com sobrepeso")
elif (imc <= 40):
    print("Você está com obesidade")
else:
    print("Você está com obesidade mórbida")