print("Verificar um número.")

print("Informe um número: ")
num = int(input())

if (num == 5):
    print(str(num) + " é igual a 5")
elif (num == 200):
    print(str(num) + " é igual a 200")
elif (num == 400):
    print(str(num) + " é igual a 400")
elif (num >= 500 and num <= 1000):
    print(str(num) + " está entre 500 e 1000")
else:
    print(str(num) + " está fora dos escopos")