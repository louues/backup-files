print("Calcular aumento de preco do produto.")

print("Informe o preco do produto: ")
preco = float(input())

if (preco <= 50):
    preco = preco * 1.05
elif (preco <= 100):
    preco = preco * 1.10
else:
    preco = preco * 1.15

preco = round(preco, 2)
print("O novo preco é de " + str(preco))

if (preco <= 80):
    print("O preco do produto é barato")
elif (preco <= 120):
    print("O preco do produto é normal")
elif (preco <= 200):
    print("O preco do produto é caro")
else:
    print("O preco do produto é muito caro")