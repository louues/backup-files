print("Verificar se é divisível por 10, 5 ou 2.")

print("Informe um número: ")
num = int(input())

if (num % 10 == 0):
    print(str(num) + " é divisível por 10")
elif (num % 5 == 0):
    print(str(num) + " é divisível por 5")
elif (num % 2 == 0):
    print(str(num) + " é divisível por 2")
else:
    print(str(num) + " não é divisível por nenhum desses")
