print("Verificar o grupo de risco.")

print("Idade: ")
idade = int(input())
print("Peso: ")
peso = float(input())

if (idade < 20):
    if (peso < 60):
        print("Grupo de risco 9")
    elif (peso <= 90):
        print("Grupo de risco 8")
    else:
        print("Grupo de risco 7")
elif (idade <= 50):
    if (peso < 60):
        print("Grupo de risco 6")
    elif (peso <= 90):
        print("Grupo de risco 5")
    else:
        print("Grupo de risco 4")
else:
    if (peso < 60):
        print("Grupo de risco 3")
    elif (peso <= 90):
        print("Grupo de risco 2")
    else:
        print("Grupo de risco 1")