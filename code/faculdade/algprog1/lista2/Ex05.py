print("Ordenar x, y e z por valor.")

print("Informe 3 números(x, y, z): ")
numeros = input().split(",")
x = int(numeros[0])
y = int(numeros[1])
z = int(numeros[2])

num1 = x
num2 = y
num3 = z

if (num1 > num2 and num1 > num3):
    if(num2 > num3):
        x = num3
        y = num2
        z = num1
    if (num3 > num2):
        x = num2
        y = num3
        z = num1
elif (num2 > num1 and num2 > num3):
    if(num1 > num3):
        x = num3
        y = num1
        z = num2
    if(num3 > num1):
        x = num1
        y = num3
        z = num2
else:
    if(num1 > num2):
        x = num2
        y = num1
        z = num3
    if(num2 > num1):
        x = num1
        y = num2
        z = num3

print("Os números em ordem são: " + str(x) + ", " + str(y) + " e " + str(z))