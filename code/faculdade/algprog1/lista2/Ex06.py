print("Somar dois hórarios.")

def format_number(num):
    if (num < 10):
        num = "0" + str(num)
    else:
        num = str(num)
    return num

print("Hórario 1(hh:mm:ss): ")
horario1 = input().split(":")
h1 = int(horario1[0])
m1 = int(horario1[1])
s1 = int(horario1[2])

print("Hórario 2(hh:mm:ss): ")
horario2 = input().split(":")
h2 = int(horario2[0])
m2 = int(horario2[1])
s2 = int(horario2[2])

s = s1 + s2
m = m1 + m2
h = h1 + h2

if (s > 60):
    s = s % 60
    m = m + 1
if (m > 60): 
    m = m % 60
    h = h + 1

h = format_number(h)
m = format_number(m)
s = format_number(s)

horario = h + ":" + m + ":" + s
print("Soma dos horários: " + horario)
