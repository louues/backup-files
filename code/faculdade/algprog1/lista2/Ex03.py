print("Verificar se é par ou ímpar.")

print("Informe o número: ")
num = int(input())

msg = "O número " + str(num) + " é "

if (num % 2 == 0):
    print(msg + "par")
else: 
    print(msg + "ímpar")