print("Verificar se um número é positivo ou negativo.")

print("Infome um número: ")
num = float(input())

msg = "O número " + str(num) + " é "

if (num > 0):
    print(msg + "positivo")
elif (num < 0):
    print(msg + "negativo")
else:
    print(msg + "igual a zero")
