print("Funcão: p(x) = 3x³ - 5x² + 2x - 1")

print("X: ")
x = float(input())

termo1 = 3 * (x*x*x)
termo2 = -5 * (x*x)
termo3 = 2 * x
termo4 = -1
res = termo1 + termo2 + termo3 + termo4

print("f(x) é igual a " + str(res))